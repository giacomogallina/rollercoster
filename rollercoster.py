#!/usr/bin/python
import csv
from math import sin, cos, tan, atan2, pi
import pygame
import sys

W, H = 1920, 1080

pygame.init()
pygame.font.init()
screen = pygame.display.set_mode((W, H))

white = pygame.Color(220, 220, 220)
yellow = pygame.Color(255, 255, 0)
red = pygame.Color(255, 0, 0)
light_grey = pygame.Color(150, 150, 150)
grey = pygame.Color(120, 120, 120)
dark_grey = pygame.Color(90, 90, 90)
black = pygame.Color(30, 30, 30)
screen.fill(light_grey)

def mul(vec, s):
    r = list(s*i for i in vec)
    return r

def add(veca, vecb):
    return list(a + b for a, b in zip(veca, vecb))

def from_axis(vec, axis):
    r = [0, 0, 0]
    for i, a in zip(vec, axis):
        r = add(r, mul(a, i))
    return r

def dot(veca, vecb):
    return sum(a * b for a, b in zip(veca, vecb))

def rotate_around_axis(a, vec, t):
    return [[vec[0], cos(t)*vec[1] - sin(t)*vec[2], sin(t)*vec[1] + cos(t)*vec[2]],
    [sin(t)*vec[2] + cos(t)*vec[0], vec[1], cos(t)*vec[2] - sin(t)*vec[0]],
    [cos(t)*vec[0] - sin(t)*vec[1], sin(t)*vec[0] + cos(t)*vec[1], vec[2]]][a]

    #[[cos(t), -sin(t), 0], [sin(t), cos(t), 0], [0, 0, 1]]

def cross(veca, vecb):
    xa, ya, za = veca
    xb, yb, zb = vecb
    return [ya*zb - yb*za, za*xb - zb*xa, xa*yb - xb*ya]

# a: vector to rotate,
# n: unit vector around which a is rotated,
# t: angle theta of rotation
def rotate_around(a, n, t): 
    return add(add(mul(n, dot(a, n)), mul(add(a, mul(n, -dot(a, n))), cos(t))), mul(cross(n, a), sin(t)))



xyz = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]

#maybe decent for small angles
def rotate(vec, t):
    return from_axis(vec, list(rotate_around_axis(2, i, t[2]) for i in list(rotate_around_axis(1, i, t[1]) for i in list(rotate_around_axis(0, i, t[0]) for i in xyz))))

def norm(vec):
    return mul(vec, 1/dot(vec, vec))





flt = lambda s: float(s)

record = []        
csvfile = open(sys.argv[1])

reader = csv.reader(csvfile)
next(reader)
for l in reader:
    l = list(map(flt, l))
    record.append([l[0], l[1:4], l[7:10]])

#print(record[0:10])


c = 0
i = 0
n = 100

while c < n:
    w = record[i][2]
    #print(w)
    if w[0]**2 + w[1]**2 + w[2]**2 < 0.0001:
        c += 1
    else:
        c = 0
    i += 1
i -= n
    
print(record[i])

g = [0, 0, 0]
for j in range(i, i+n):
    for k in range(3):
        g[k] += record[j][1][k]
for k in range(3):
    g[k] /= n
    
print("g (from phone):", g)

t = atan2(g[1], g[0])
g1 = from_axis(g, list(rotate_around_axis(2, i, -t) for i in xyz))
t1 = atan2(g1[0], g1[2])

axis = list(rotate_around_axis(1, i, -t1) for i in list(rotate_around_axis(2, i, -t) for i in xyz))
# axis = xyz

print("axis:", axis)
print("g (from world):", from_axis(g, axis))

v = [0, 0, 0]
p = [[0, 0, 0]]

project = [[-0.866, 0.5, 0], [0.866, 0.5, 0], [0, -1, 0]]
shadow = [[-0.866, -0.5, 0], [0.866, -0.5, 0], [0, 0, 0]]

rgb = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
while i < len(record):
    dt = record[i][0] - record[i-1][0]
    t = dot(record[i][2], record[i][2])
    if t > 0.001:
        wn = mul(record[i][2], 1/t)
        axis = list(rotate_around(a, from_axis(wn, axis), t*dt) for a in axis)
    #axis = list(from_axis(a, axis) for a in list(rotate(a, mul(record[i][2], -dt)) for a in xyz))
    v = add(v, mul(from_axis(record[i][1], axis), dt))
    v = add(v, mul([0, 0, -1], dt))
    p.append(add(p[-1], mul(v, dt)))


    if i % 10 == 0:
        print(axis)
        # screen.fill(light_grey)
        pp = mul(from_axis(p[-1], project), 3)
        ps = mul(from_axis(p[-1], shadow), 3)
        screen.set_at((int(pp[0])+W//2, int(pp[1])+H//2), (abs(record[i][1][0])*40, abs(record[i][1][1])*40, abs(record[i][1][2])*40,))
        screen.set_at((int(ps[0])+W//2, int(ps[1])+H//2), dark_grey)
        # for a, c in zip(axis, rgb):
            # pa = mul(from_axis(a, project), 300)
            # pa = (int(pa[0]+W/2), int(pa[1]+H/2))
            # print(pa)
            # pygame.draw.line(screen, c, (W//2, H//2), pa, 3)
            # screen.set_at(pa, black)
        pygame.display.update()

    i += 1





